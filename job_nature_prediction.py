# purpose of this program is to return job_nature for a given job title and job description.

import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
from flask import Flask, request, jsonify
import ast
#nltk.download("stopwords")


def read_dict(file_name):

    """
    This function reads the json file and store the dictionary
    """

    test_file = open(file_name, "r")
    test_dict = test_file.read()
    
    test_file.close()

    return test_dict
    
def clean_text(text):

    """
    This function will be used to clean the text

    text: uncleaned text
    return: cleaned text
    """

    stop_words = set(stopwords.words('english'))
    text = text.lower()     
    clean = re.compile('<.*?>')
    text = re.sub(clean, '', text)     #removes all the html tags from the text if present
    text = re.sub("[^/a-zA-Z']", " ", text) #removes any special character from the text 
    text = re.sub(' +', ' ', text)
    text = text.split(" ")               
    text = " ".join([w for w in text if not w in stop_words]) #removing stopwords from the text

    return text

def merge_title_description(job_title, job_description):

    description_title = clean_text(job_description) + " " + clean_text(job_title)
    description_title = description_title.split(" ")
    return description_title

def job_likelihood(description_title, part_likelihood, full_likelihood):

    """ 
    This function calculates the part time likelihood and full time likelihood of each word in job 
    description and title
    """
    job_part_likelihood=[]

    for word in description_title:
            if word in part_likelihood.keys():
                job_part_likelihood.append(part_likelihood[word])

    job_full_likelihood=[]

    for word in description_title:
            if word in full_likelihood.keys():
                job_full_likelihood.append(full_likelihood[word])
                
    return job_part_likelihood, job_full_likelihood
        
def product_list(element_list):
    """
    Gives product of all the elements in the list
    """

    product=1
    for item in element_list:
        product *= item
    
    return product

def estimate_nature(job_part_likelihood, job_full_likelihood):
    """
    Estimates the nature of the jobs by multiplying the likelihood of all the words
    and comparing them for full time and part time jobs
    """

    part_likelihood_product = product_list(job_part_likelihood)
    full_likelihood_product = product_list(job_full_likelihood)

    if part_likelihood_product > full_likelihood_product:
        return "Part Time"
    else:
        return "Full Time"

def get_job_nature(job_title, job_description):

    # merge job_title and job description
    description_title = merge_title_description(job_title, job_description)

    # evalute part-time likelihood
    part_likelihood = ast.literal_eval(read_dict('part_likelihood.json'))

    # evaluate full-time likelihood
    full_likelihood = ast.literal_eval(read_dict('full_likelihood.json'))

    job_part_likelihood, job_full_likelihood = job_likelihood(description_title, part_likelihood, full_likelihood)
    nature = estimate_nature(job_part_likelihood, job_full_likelihood)

    return nature
    
app = Flask(__name__)
@app.route('/',methods = ['POST', 'GET'])

def job_info():

    job_title = request.form.get("job_title")
    job_description = request.form.get("job_description")

    #get job nature
    job_nature = get_job_nature(job_title, job_description)
    result_dict = {"job_nature" : job_nature}
    
    return jsonify(result_dict)

if __name__ == "__main__":
    app.run(port=6001)
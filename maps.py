# The purpose of this code to calculate likelihood of different words for part-time and full-time jobs
# This will further be used to predict the nature of the jobs (wheter it's full time or part time) using bayesian likelihood

#Importing libraries 
import re
import os
import nltk
import json
from nltk.corpus import stopwords
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
import pymysql
#nltk.download("stopwords")

def clean_text(text):

    """
    This function will be used to clean the text
    text: uncleaned text
    return: cleaned text
    """
    
    stop_words = set(stopwords.words('english'))
    text = text.lower()     
    clean = re.compile('<.*?>')
    text = re.sub(clean, '', text)     #removes all the html tags from the text if present
    text = re.sub("[^/a-zA-Z']", " ", text) #removes any special character from the text 
    text = re.sub(' +', ' ', text)
    text = text.split(" ")               
    text = " ".join([w for w in text if not w in stop_words]) #removing stopwords from the text

    return text

def get_connection():

    #get environment variable
    user=os.getenv("DBUSER")
    passwd=os.getenv("DBPASS")
    host=os.getenv("DBHOST")
    database=os.getenv("DBNAME")

    #connection string
    con_string="mysql+pymysql://{}:{}@{}/{}".format(user, passwd, host, database)

    #create engine
    sqlEngine = create_engine(con_string, pool_recycle=3600)
    db = sqlEngine.raw_connection()
    return db

def get_jobs():
    """
    This function first loads the data from the database and then make some necessary changes.

    return: processed dataset with necessary columns
    """

    db = get_connection()
    cur = db.cursor()

    query = """select html_job_description, job_title, estimated_job_nature from job where estimated_job_nature 
    in ('full time', 'part time')"""
    cur.execute(query)

    job_list = cur.fetchall()
    cur.close()
    db.close()

    df = pd.DataFrame(list(job_list))
    df.columns = ["job_description", "job_title", "job_nature"]
    
    #joining the title and the description and making a  new column called title_description and 
    #making a new column called title_description
    df["title_description"] = df['job_title'] + ' ' + df['job_description'] 

    #cleaning the title_description column using the clean text function created above
    df["title_description"] = [clean_text(i) for i in df["title_description"]]

    #splitting the title_description to list
    df["title_description"] = df["title_description"].str.split(" ")   

    df.dropna(subset=["job_nature", "title_description"], inplace=True) 

    #Dropping duplicate and null values.       
    df.reset_index(inplace = True, drop = True)

    return df



def get_map(df):

    """
    This function calculates the frequency of each word using in title and descrition for all the jobs,
    part time jobs and full time jobs.

    input: dataframe
    return: word_map (frequency of each word in all the jobs),
            part_map (frequency of each word in only the part time jobs),
            full_map (frequency of each word in only the full time jobs)
    """

    word_map = {}

    for title_description in df.title_description:
        for word in title_description:

            #Here, we will create a dictionary in form {word: frequency} for all the jobs
            if word not in word_map.keys():
                word_map[word] = 1

            else:
                word_map[word] += 1
                
    part_map = {}

    for title_description in df[df.job_nature=="part time"]["title_description"]:
        for word in title_description:

            #Here, we will create a dictionary in form {word: frequency} for only the part time jobs
            if word not in part_map.keys():
                part_map[word] = 1

            else:
                part_map[word] += 1

    full_map = {}

    for title_description in df[df.job_nature=="full time"]["title_description"]:
        for word in title_description:
            
            #Here, we will create a dictionary in form {word: frequency} for only the full time jobs
            if word not in full_map.keys():
                full_map[word] = 1

            else:
                full_map[word] += 1
    
    return word_map, part_map, full_map

def likelihood_dictionary(word_map, part_map, full_map):
    """
    This function is used to calculate the part-likelihood and full-likelihood of each word.
    Part-Likelihood is the ratio of frequency of a word in part time jobs to frequency of a word in all the jobs.
    Full-Likelihood is the ratio of frequency of a word in full time jobs to frequency of a word in all the jobs.

    input: word_map, part_map, full_map
    return: part_likelihood (a word's likelihood for a part-time jobs) (dictionary of form {word: part_time_likelihood})
            full_likelihood (a word's likelihood for a full-time jobs) (dictionary of form {word: full_time_likelihood})
    """
    part_likelihood = {}
    full_likelihood = {}

    for word,frequency in word_map.items():

        if word in part_map.keys():

            #Dividing word's frequency in part time jobs to it's frequency in all the jobs
            part_likelihood[word] = part_map[word]/frequency  
        
        else:
            part_likelihood[word] = 0


        if word in full_map.keys():

             #Dividing word's frequency in full time jobs to it's frequency in all the jobs
            full_likelihood[word] = full_map[word]/frequency 
        
        else:
            full_likelihood[word] = 0
    
    return part_likelihood, full_likelihood

def write_dict_to_file(dict, file_name):
    """
    This function writes the dictionary to json format file of given name.

    input: dictionary, fill name
    """
    file = open(file_name, 'w')
    json.dump(dict, file)
    file.close()

def main(): 

    df=get_jobs()  #loading the data
    word_map, part_map, full_map = get_map(df) #Getting the frequency maps
    part_likelihood, full_likelihood = likelihood_dictionary(word_map, part_map, full_map) #Getting the likelihood

    #Writing the part time likelihood dictionary to file
    write_dict_to_file(part_likelihood, "part_likelihood.json")  

    #Writing the full time likelihood dictionary to file
    write_dict_to_file(full_likelihood, "full_likelihood.json") 

if __name__ == "__main__":
    main()

# Project Name
Bayesian ikelihood estimator for determining per word contribution towards job nature and predicting the job nature.


# Project Description
The idea behind this project was to caculate per word contribution towards thejob nature. Firstly, we took all the words from all the jobs (title and description) and made a word map which contained all the words and their frequency. Next we did the same for part time and full time jobs, by creating part_map and full_map that have frequency of words used in all the part time and full time job respectively. Then we caclulated the part and full likelihood of eachword using:

part_likelihood[word] = part_map[word]/word_map[word]
full_likelihood[word] = full_map[word]/word_map[word]

We made the dictionary part_likelihood and full_likelihood that contain the part-time and full-time likelihood of each word. Next, we used these dictionary to calculate the part-likehood and full-lieklihood of the job by multiplying the part-likelihood and full-likelihood of each word in the job description. Now, if the job's part-likelihood > job's full-likelihood, it's lablelled as part time as vice-versa.

# Project Roadmap
1. Setting up a virtual environment and installing all the packages required (given in the requirement.txt)
2. Executing the maps.py file, which will give us two json format file that will contain the part_likelihood and full_likelihood of each word.
3. Executing the job_nature_prediction.py which contains api code to predict job_nature from job_description and job_title.

# Step by Step code

### 1. Setting up a virtual environment
```
python3 -m venv myvenv
source myvenv/bin/activate
```
### 2. Installing required python libraries
```
pip install -r requirements.txt
```
### 3. Create  environment variable file
#### Filename env.sh contains the following credentials
```
export ENDPOINT_ID="XXXXXXXXXXXXXXXXXXX"
export PROJECT_ID="XXXXXXXXXXXXXXXXX"
export DBHOST="localhost"
export DBUSER="root"
export DBNAME="XXXXX"
export DBPASSWORD="XXXXXXXX"
```

### 4. Set environment variable

```source env.sh```

### 5. Get Likelihood dictionaries
```
python3 maps.py
```

### 6.Run API
```
python3 job_nature_prediction.py
```

Input : job_title, job_description

Output : job_nature

e.g 

Input:
```{job_title: "full time software engineer", job_description: "required an experience engineer"}```

Output: 
```{"job_nature": "Full Time"}```
